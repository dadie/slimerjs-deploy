# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

if(CPACK_GENERATOR STREQUAL "DEB")
  set(CPACK_COMPONENTS_ALL unix)
  set(CPACK_DEB_COMPONENT_INSTALL ON)
  set(CPACK_DEBIAN_UNIX_FILE_NAME "${CPACK_PACKAGE_FILE_NAME}.deb")
  set(CPACK_DEBIAN_UNIX_PACKAGE_NAME "${CPACK_PACKAGE_NAME}")
elseif(CPACK_GENERATOR MATCHES "^NSIS")
  set(CPACK_COMPONENTS_ALL core optional)
  set(SLIMERJS_ENV "slimerjs_env")
  if(DEFINED CPACK_NSIS_INSTALLED_ICON_NAME)
    set(SLIMERJS_ENV_ICON "\$INSTDIR\\${CPACK_NSIS_INSTALLED_ICON_NAME}")
  endif()

  # Adapt description and display name of the components to local
  # language and preferences
  foreach(component IN LISTS CPACK_COMPONENTS_ALL)
    string(TOUPPER "${component}" component)
    set("CPACK_COMPONENT_${component}_DISPLAY_NAME"
      "\$(CPACK_COMPONENT_${component}_DISPLAY_NAME)")
    set("CPACK_COMPONENT_${component}_DESCRIPTION"
      "\$(CPACK_COMPONENT_${component}_DESCRIPTION)")
  endforeach()

  set(CPACK_NSIS_EXTRA_PREINSTALL_COMMANDS "
  Push \$0
  ClearErrors
  FileOpen \$0 '\$INSTDIR\\${SLIMERJS_ENV}.cmd' w
  \${IfNot} \${Errors}
    FileWrite \$0 '@echo off\$\\r\$\\ntitle SlimerJS\$\\r\$\\n'
    \${IfNot} \${SectionIsSelected} \${optional}
      FileWrite \$0 'rem '
    \${EndIf}
    FileWrite \$0 'doskey slimerjs=\"%SystemRoot%\\system32\\cscript\" \"\$INSTDIR\\slimerjs.js\" \$\$*\$\\r\$\\n'
    FileClose \$0
  \${EndIf}
  ClearErrors
  Pop \$0
  ")

  set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "
  Push \$0
  Push \$1
  \${If} '\$STARTMENU_FOLDER' != ''
    StrCpy \$0 '\$SMPROGRAMS\\\$STARTMENU_FOLDER'
  \${Else}
    StrCpy \$0 '\$INSTDIR'
  \${EndIf}
  ; Prefer powershell
  StrCpy \$1 '\$SYSDIR\\WindowsPowerShell\\v1.0\\powershell.exe'
  \${If} \${FileExists} \$1
    CreateShortcut \\
      '\$0\\${SLIMERJS_ENV}.lnk' '\$1' \\
      '-NoLogo -NoExit -Command \"function slimerjs {& \\\"\$\${ENV:\\SystemRoot}\\system32\\cscript.exe\\\" \\\"\$INSTDIR\\slimerjs.js\\\" @args};\$\$host.ui.RawUI.WindowTitle=\\\"SlimerJS\\\"\"' \\
      '${SLIMERJS_ENV_ICON}'
  \${Else}
    ExpandEnvStrings \$1 '%COMSPEC%'
    CreateShortcut '\$0\\${SLIMERJS_ENV}.lnk' '\$1' \\
      '/K \"\$INSTDIR\\${SLIMERJS_ENV}.cmd\"' \\
      '${SLIMERJS_ENV_ICON}'
  \${EndIf}
  Pop \$1
  Pop \$0
  ")

  set(CPACK_NSIS_INSTALLER_MUI_FINISHPAGE_RUN_CODE "
\${ExecLinkAsUser}
Function slimerjs_env
  Push \$0
  \${If} '\$STARTMENU_FOLDER' != ''
    StrCpy \$0 '\$SMPROGRAMS\\\$STARTMENU_FOLDER'
  \${Else}
    StrCpy \$0 '\$INSTDIR'
  \${EndIf}
  \${ExecLinkAsUser} '\$0\\${SLIMERJS_ENV}.lnk'
  Pop \$0
FunctionEnd

!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_FUNCTION slimerjs_env
")

  set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "
  \${If} '\$STARTMENU_FOLDER' != ''
    Delete '\$SMPROGRAMS\\\$STARTMENU_FOLDER\\${SLIMERJS_ENV}.lnk'
  \${Else}
    Delete '\$INSTDIR\\${SLIMERJS_ENV}.lnk'
  \${EndIf}
  Delete '\$INSTDIR\\${SLIMERJS_ENV}.cmd'
  Pop \$0
  ")
else()
  set(CPACK_COMPONENTS_ALL core)
  set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)
endif()
