# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Functions to convert portable object files into a NSIS specific language
# files

# Escape language string
# Parameter:
#   langstring language string
function(escape_langstring langstring)
  string(REGEX REPLACE "(^|[^$])(\\\\[nrt])" "\\1$\\2" ${langstring} "${${langstring}}")
  set(${langstring} "${${langstring}}" PARENT_SCOPE)
endfunction()

# Write language string
# Parameters:
#   dest complete path of destination file
#   msg_ctxt message context
#   msg_str translated string
function(write_langstring dest msg_ctxt msg_str)
  escape_langstring(${msg_str})
  file(APPEND ${dest} "\${LangFileString} ${msg_ctxt} \"${msg_str}\"\n")
endfunction()

# Export language string for use in CMake
# Parameters:
#   export flag whether to export or not
#   msg_ctxt message context
#   msg_str translated string
function(export_cpack_langstring export msg_ctxt msg_str)
  if(export)
    if(${msg_ctxt} MATCHES "^CPACK_")
      set(${msg_ctxt} ${msg_str} CACHE STRING ${msg_ctxt})
    endif()
  endif()
endfunction()

# Create language file
# Parameters:
#   lang language name as used in NSIS
#   src complete path of source file
#   dest complete path of destination file
#   export flag whether to export or not
# Remarks:
#   Converts a portable object file into a NSIS specific language file
function(create_langfile lang src dest export)
  file(READ ${src} content)
  # Convert content into a CMake list
  string(REGEX REPLACE ";" "\\\\;" content ${content})
  string(REGEX REPLACE "\n" ";" content ${content})
  string(ASCII 239 187 191 UTF8_BOM)
  file(WRITE ${dest} "${UTF8_BOM}!insertmacro LANGFILE_EXT '${lang}'\n")
  set(n 0)
  foreach(line IN LISTS content)
    string(STRIP "${line}" line)
    if(${line} MATCHES "^(|msg(ctxt|id|str)[ \t]+)\"(.*)\"")
      if(CMAKE_MATCH_2)
        if(${CMAKE_MATCH_2} STREQUAL "ctxt")
          if(msg_ctxt AND msg_str)
            write_langstring(${dest} ${msg_ctxt} ${msg_str})
            export_cpack_langstring(${export} ${msg_ctxt} ${msg_str})
            math(EXPR n "${n} + 1")
          endif()
          unset(msg_id)
          unset(msg_str)
        elseif(${CMAKE_MATCH_2} STREQUAL "id")
          unset(msg_str)
        endif()
        set(msg_current ${CMAKE_MATCH_2})
        set(msg_${CMAKE_MATCH_2} ${CMAKE_MATCH_3})
      else()
        if(msg_current)
          string(CONCAT msg_${msg_current} ${msg_${msg_current}} ${CMAKE_MATCH_3})
        endif()
      endif()
    else()
      if(NOT ${line} MATCHES "^#")
        unset(msg_current)
      endif()
    endif()
  endforeach()
  if(msg_ctxt AND msg_str)
    write_langstring(${dest} ${msg_ctxt} ${msg_str})
    export_cpack_langstring(${export} ${msg_ctxt} ${msg_str})
    math(EXPR n "${n} + 1")
  endif()
  if(n LESS 1)
    # Remove file without any translations
    file(REMOVE ${dest})
  endif()
endfunction()

# Create language files
# Parameters:
#   out_dir directory where the generated files are going to be stored
#   locales_dir complete path of locales directory
#   potfile name of Portable Object Template file located in locales_dir
# Remarks:
#   The locales directory accomodates the POT file and directories
#   containing the translations. The translation files have to be named
#   as the base name of the pot file but have the extension .po instead
#   of .pot.
#   The translation files are converted to NSIS specific language file
#   and the header file languages.nsh is generated.
function(create_langfiles out_dir locales_dir potfile)
  set(headerfile "${out_dir}/languages.nsh")
  # Language mappings
  set(af_ZA "Afrikaans")
  set(sq_AL "Albanian")
  set(ar_SA "Arabic")
  set(hy_AM "Armenian")
  set(ast_ES "Asturian")
  set(eu_ES "Basque")
  set(be_BY "Belarusian")
  set(bs_BA@latin "Bosnian")
  set(br_FR "Breton")
  set(bg_BG "Bulgarian")
  set(ca_ES "Catalan")
  set(co_FR "Corsican")
  set(hr_HR "Croatian")
  set(cs_CZ "Czech")
  set(da_DK "Danish")
  set(nl_NL "Dutch")
  set(en_US "English")
  set(eo "Esperanto")
  set(et_EE "Estonian")
  set(fa_IR "Farsi")
  set(fi_FI "Finnish")
  set(fr_FR "French")
  set(gl_ES "Galician")
  set(ka_GE "Georgian")
  set(de_DE "German")
  set(el_GR "Greek")
  set(he_IL "Hebrew")
  set(hu_HU "Hungarian")
  set(is_IS "Icelandic")
  set(id_ID "Indonesian")
  set(ga_IE "Irish")
  set(it_IT "Italian")
  set(ja_JP "Japanese")
  set(ko_KR "Korean")
  set(ku_IQ "Kurdish")
  set(lv_LV "Latvian")
  set(lt_LT "Lithuanian")
  set(de_LU "Luxembourgish")
  set(mk_MK "Macedonian")
  set(ms_MY "Malay")
  set(mn_MN "Mongolian")
  set(nb_NO "Norwegian")
  set(nn_NO "NorwegianNynorsk")
  set(ps_AF "Pashto")
  set(pl_PL "Polish")
  set(pt_PT "Portuguese")
  set(pt_BR "PortugueseBR")
  set(ro_RO "Romanian")
  set(ru_RU "Russian")
  set(gd_GB "ScotsGaelic")
  set(sr_RS "Serbian")
  set(sr_RS@latin "SerbianLatin")
  set(zh_CN "SimpChinese")
  set(sk_SK "Slovak")
  set(sl_SI "Slovenian")
  set(es_ES "Spanish")
  set(sv_SE "Swedish")
  set(th_TH "Thai")
  set(zh_TW "TradChinese")
  set(tr_TR "Turkish")
  set(uk_UA "Ukrainian")
  set(uz_UZ@latin "Uzbek")
  set(vi_VN "Vietnamese")
  set(cy_GB "Welsh")

  set(lc "en_US")
  create_langfile(${${lc}} "${locales_dir}/${potfile}" "${out_dir}/${${lc}}.nsh" TRUE)
  file(WRITE ${headerfile} "!insertmacro ADD_LANGUAGE '${${lc}}'\n")
  string(REGEX REPLACE "\\.pot$" ".po" pofile ${potfile})

  file(GLOB locales LIST_DIRECTORIES TRUE
    RELATIVE ${locales_dir} "${locales_dir}/[!.]*")
  list(REMOVE_ITEM locales ${potfile})
  foreach(lc IN LISTS locales)
    if(${lc})
      create_langfile(${${lc}} "${locales_dir}/${lc}/${pofile}" "${out_dir}/${${lc}}.nsh" FALSE)
      list(APPEND languages ${${lc}})
    endif()
  endforeach()

  list(SORT languages)
  foreach(lang IN LISTS languages)
    file(APPEND ${headerfile} "!insertmacro ADD_LANGUAGE '${lang}'\n")
  endforeach()
endfunction()
