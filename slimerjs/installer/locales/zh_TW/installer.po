# Copyright (C) 2017  Thomas Gaugler <thomas@dadie.net>
# This file is distributed under the same license as the
# slimerjs-deploy package.
# 2017 Thomas Gaugler <thomas@dadie.net>
#
# Custom strings for slimerjs installer
#
# Remarks:
# * Double quotes (") must not be used in translated text
# * Leave $(^...) $... strings untouched
msgid ""
msgstr ""
"Project-Id-Version: slimerjs-deploy 1.0.0-alpha.2-pre\n"
"POT-Creation-Date: 2017-10-11 21:04+0200\n"
"PO-Revision-Date: 2017-10-11 22:03+0200\n"
"Last-Translator: FULL NAME <translator@example.com>\n"
"Language-Team: LANGUAGE <team@example.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

msgctxt "ALREADY_INSTALLED_CAPTION"
msgid "$(^NameDA) is already installed"
msgstr ""

msgctxt "ALREADY_INSTALLED_SUBCAPTION"
msgid "Do you want to uninstall the existing version?"
msgstr ""

msgctxt "TARGET_DIRECTORY_EXISTS"
msgid "$INSTDIR does already exist. Please choose a different folder."
msgstr ""

msgctxt "CPACK_PACKAGE_DESCRIPTION_SUMMARY"
msgid "A scriptable browser like PhantomJS, based on Firefox"
msgstr ""

msgctxt "CPACK_COMPONENT_CORE_DISPLAY_NAME"
msgid "$(^NameDA)"
msgstr ""

msgctxt "CPACK_COMPONENT_CORE_DESCRIPTION"
msgid "Scriptable browser utilizing Firefox"
msgstr ""

msgctxt "CPACK_COMPONENT_OPTIONAL_DISPLAY_NAME"
msgid "Launch script"
msgstr ""

msgctxt "CPACK_COMPONENT_OPTIONAL_DESCRIPTION"
msgid ""
"Launch script targetting CScript (command-line version of the Windows Script "
"Host)"
msgstr ""
