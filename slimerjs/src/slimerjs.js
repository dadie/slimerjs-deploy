/*
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Usage:
 *
 * cscript.exe [options] slimerjs.js
 *
 * Example:
 * cscript slimerjs.js -jsconsole scripts\goto.js http://localhost:8080
 */

'use strict';

/**
 * Get path to Mozilla Firefox executable
 */
function getPathToFirefox() {
  var path = null;
  var HKLM = 0x80000002;
  var FF_KEY = 'SOFTWARE\\Mozilla\\Mozilla Firefox';
  var suffixes = [' ESR', ''];
  var objCtx32bitArch;
  var objLocator = WScript.CreateObject('Wbemscripting.SWbemLocator');
  var objServices;
  var params;
  var result;
  var objCtx32bitArch =
      WScript.CreateObject('WbemScripting.SWbemNamedValueSet');
  objCtx32bitArch.Add('__ProviderArchitecture', 32);
  var objCtxArray = new Array(null, objCtx32bitArch);
  var objCtx;
  var suffix;
  do {
    suffix = suffixes.shift();
    for (var i in objCtxArray) {
      objCtx = objCtxArray[i];
      objServices = objLocator.ConnectServer('', 'root\\default', '', '', null,
                                             null, null, objCtx);
      objStdRegProv = objServices.Get('StdRegProv');
      params = objStdRegProv.Methods_('GetStringValue').Inparameters;
      params.Hdefkey = HKLM;
      params.Ssubkeyname = FF_KEY + suffix;
      params.Svaluename = null;
      result = objStdRegProv.ExecMethod_('GetStringValue', params, null, objCtx);
      if (result.SValue != null) {
        params.Ssubkeyname = FF_KEY + ' ' + result.SValue + suffix + '\\bin';
        params.Svaluename = 'PathToExe';
        result =
            objStdRegProv.ExecMethod_('GetStringValue', params, null, objCtx);
        if (result.SValue != null) {
          path = result.SValue;
        }
        break;
      }
    }
  } while (!path && suffix);
  return path;
}

var shell = WScript.CreateObject('WScript.Shell');
var env = shell.Environment('Process');
var fso = WScript.CreateObject('Scripting.FileSystemObject');
var args = '', item, iterator, list = '', path;
var profile_opt = [ '--profile', '-profile', '--p', '-p' ];

/* Pass list of environment variables to slimerjs via __SLIMER_ENV environment
 * variable
 */
iterator = new Enumerator(env);
while (!iterator.atEnd()) {
  item = iterator.item();
  item = item.slice(0, item.indexOf('='));
  if (item.length > 0) {
    list += ',' + item;
  }
  iterator.moveNext();
}
env('__SLIMER_ENV') = list.slice(1);

/* Pass arguments to slimerjs via __SLIMER_ARGS environment variable */
iterator = new Enumerator(WScript.Arguments);
while (!iterator.atEnd()) {
  item = iterator.item();
  if (item.indexOf(' ') < 0) {
    args += ' ' + iterator.item();
  } else {
    /* Put argument in quotation marks because it contains a space character */
    args += ' "' + iterator.item() + '"';
  }
  item = item.toLowerCase();
  for (var i in profile_opt) {
    if (item == profile_opt[i]) {
      path = true;
      break;
    }
  }
  iterator.moveNext();
}
/* Configure profile path in case none was provided */
if (path) {
  args = args.slice(1);
} else {
  path = shell.SpecialFolders('AppData');
  if (path) {
    path += '\\';
  }
  path += 'slimerjs';
  args = profile_opt[0] + ' "' + path + '"' + args;
}
env('__SLIMER_ARGS') = args;

/* Take application.ini from the same folder where this script resides */
path = fso.GetParentFolderName(fso.GetFile(WScript.ScriptFullName));
if (path) {
  path += '\\';
}
path += 'application.ini';

var cmd = shell.Exec(
    '"' + env('COMSPEC') + '" /D /C ""' + getPathToFirefox() + '" -app "' +
    path + '" -no-remote -attach-console -purgecaches ' + args + ' 2>&1"');

while (!cmd.StdOut.AtEndOfStream) {
  WScript.Echo(cmd.StdOut.ReadLine());
}
