**This repository is no longer maintained.**

[Puppeteer](https://https://pptr.dev) would be an alternative to this project. The below example illustrates the usage of puppeteer.

```javascript
/*
  mkdir automate
  cd automate
  yarn init
  PUPPETEER_PRODUCT=firefox yarn add puppeteer
  node index.js

  index.js
 */ 
(async () => {
  const puppeteer = require('puppeteer')
  const browser = await puppeteer.launch({product: 'firefox'})
  const page = await browser.newPage()
  await page.goto('http://localhost:8000/')
  await page.screenshot({path: 'screenshot.png'})
  await browser.close()
})()
```
___

[SlimerJS](https://slimerjs.org) is a scriptable browser like
[PhantomJS](http://phantomjs.org). It utilizes an installed
[Mozilla Firefox](https://www.mozilla.org/firefox) browser for its
scripting.

The aim of slimerjs-deploy is to make the installation and usage
of SlimerJS as pleasant as possible.

At the moment you have to build the deployment packages yourself.

# Build

## Prerequisites

* [git](https://git-scm.com)
* [cmake](https://cmake.org) version 3.0 or newer
* [ninja-build](https://ninja-build.org)
* [Nullsoft Installer](http://nsis.sourceforge.net) version 3.0 or newer

## Instructions

```shell
mkdir sandbox
cd sandbox
git clone https://gitlab.com/dadie/slimerjs-deploy.git
mkdir build
cd build
cmake -G Ninja ../slimerjs-deploy
cmake --build .
```

# Deploy

The build produces a zip archive for UNIX-like systems and an 
installer executable for Windows.

On UNIX-like systems unpack the zip archive in a new created directory
and start the slimerjs shell script from there within a terminal.

On Windows double click on the installer executable to start the
installation. The shortcut installed in either the start menu or
installation folder provides an alias for slimerjs within the Command
Prompt (via doskey) respectively PowerShell (via function).

So you would run slimerjs with the below javascript file such as:
```shell
slimerjs hello.js
```

```javascript
console.log('Hello World');
phantom.exit();
```

# Contribute

If you like to contribute to the project then please read the
[CONTRIBUTING.md](CONTRIBUTING.md) file for further information.

# License

[Mozilla Public License Version 2.0](LICENSE.md)
